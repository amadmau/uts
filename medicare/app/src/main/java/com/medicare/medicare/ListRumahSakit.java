package com.medicare.medicare;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListRumahSakit extends AppCompatActivity {
    private static final String TAG = "ListRumahSakit";
    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_layout);
        mListView = (ListView) findViewById(R.id.listView);
        ArrayList<Card> list = new ArrayList<>();
        list.add(new Card("drawable://" + R.drawable.rs1, "Rumah Sakit Pasar Rebo"));
        list.add(new Card("drawable://" + R.drawable.rs2, "Rumah Sakit Gatoet Soebroto"));
        list.add(new Card("drawable://" + R.drawable.rs3, "Rumah Sakit Gatoet Soebroto"));
        list.add(new Card("drawable://" + R.drawable.rs4, "Rumah Sakit Gatoet Soebroto"));
        list.add(new Card("drawable://" + R.drawable.rs5, "Rumah Sakit Gatoet Soebroto"));

        CustomListAdapter adapter = new CustomListAdapter(this, R.layout.activity_list_rumah_sakit, list);
        mListView.setAdapter(adapter);

    }
}
