package com.medicare.medicare;

import android.content.Intent;
import android.os.Build;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.GridLayout;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.medicare.medicare.Utils.BottomNavigationViewHelper;

public class MainActivity extends AppCompatActivity {
    private GridLayout gridHMenu;

    private static final String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: starting.");

//       Setting buat warna Status Bar
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(getColor(R.color.statusBar));
        }
        else {
            window.setStatusBarColor(getResources().getColor(R.color.statusBar));
        }

//        panggil fungsi settup Bootom Navigation
        setupBottomNavigationView();

        gridHMenu = (GridLayout) findViewById(R.id.gridMenu);
        //buat event
        setSingleEvent(gridHMenu);
    }

    private void setupBottomNavigationView(){
        Log.d(TAG, "setupBottomNavigationView: setting up BottomNavigationView");
        BottomNavigationViewEx bnve = (BottomNavigationViewEx) findViewById(R.id.bottomNavViewBar);
        bnve.enableAnimation(false);
        bnve.enableShiftingMode(false);
        bnve.enableItemShiftingMode(false);
        bnve.setTextVisibility(false);
    }

    private void setSingleEvent(GridLayout gridHMenu) {
        //looping semua elemen di dalam gridMenun
        for (int i=0; i<gridHMenu.getChildCount();i++){
            CardView cardView = (CardView) gridHMenu.getChildAt(i);
            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (finalI == 0) {
                        Intent intent = new Intent(MainActivity.this, ListRumahSakit.class);
                        startActivity(intent);
                    }

                }
            });
        }
    }
}
